import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../services/request.service';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public data: any;
  public url: string;

  constructor(private svc: RequestService,
              private router: Router,
              private route: ActivatedRoute) {
    this.url = decodeURI(this.router.url);
    this.getData(this.url);

  }

  ngOnInit() {
  }

  getData(url = '/') {
    this.svc.getData('disk/resources', {
      'path': url
    }).pipe(map(response => {
      return response.body;
    }))
      .subscribe(res => {
        this.data = res;
      });
  }

  fixSize(val) {
    return (val / 1024).toFixed(0);
  }

  changePage(item) {
    if (item.type === 'dir') {
      const url = item.path.replace('disk:/', '');
      this.router.navigate([`/${url}`]);
    } else {
      return false;
    }
  }
}
