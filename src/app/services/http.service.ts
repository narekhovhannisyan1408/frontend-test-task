import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { throwError } from 'rxjs/index';

import {appConfig} from '../app.config';

@Injectable()
export class HttpService {

  constructor(
    private router: Router,
    private http: HttpClient,
  ) {
  }

  get(url: string, params: any = {}): Observable<HttpResponse<Object>> {
    return this.http.get<HttpResponse<Object>>(appConfig.apiUrl + url, this.addOptions(this.toHttpParams(params)))
      .catch(error => this.handleError(error));
  }
  post(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.post<HttpResponse<Object>>(appConfig.apiUrl + url, body, this.addOptions())
      .catch(error => this.handleError(error));
  }

  put(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.put<HttpResponse<Object>>(appConfig.apiUrl + url, body, this.addOptions())
      .catch(error => this.handleError(error));
  }
  delete(url: string): Observable<HttpResponse<Object>> {
    return this.http.delete<HttpResponse<Object>>(appConfig.apiUrl + url, this.addOptions())
      .catch(error => this.handleError(error));
  }
  private toHttpParams(params) {
    params = this.checkParams(params);

    return Object.getOwnPropertyNames(params)
      .reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }

  private checkParams(obj) {
    return JSON.parse(JSON.stringify(obj,
      function (k, v) {
        if (v === null) {
          return undefined;
        }
        return v;
      }
    ));
  }

  private addOptions(params?: HttpParams) {
    const options = {};
    if (params) {
      options['params'] = params;
    }
    options['headers'] = new HttpHeaders({

      'Authorization': `OAuth AQAAAAAV0SnuAAUxoAnT6yRTIkE9hBvnF4g55ZY`
    });

    options['observe'] = 'response';
    return options;
  }

  private handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        this.router.navigate(['/']);
      }

      return throwError(error);
    }
  }
}
